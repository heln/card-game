package edu.ntnu.idatt2001.a4;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class HandTest {
  Hand hand;

  @BeforeEach
  void generateHand() {
    hand = new Hand();
    hand.addCard(new PlayingCard('H', 5));
    hand.addCard(new PlayingCard('H', 12));
    hand.addCard(new PlayingCard('D', 7));
    hand.addCard(new PlayingCard('S', 1));
    hand.addCard(new PlayingCard('C', 13));
  }

  @Test
  @DisplayName("Sum should be 38")
  void sumShouldBeThirtyEight() {
    assertEquals(38, hand.getSum());
  }

  @Test
  @DisplayName("Get hearts should return H12 H5")
  void getHearts() {
    assertEquals("H12 H5", hand.getHearts());
  }

  @Test
  void getFlushShouldReturnFalse() {
    assertFalse(hand.flush());
  }

  @Test
  void getFlushShouldReturnTrue() {
    Hand hand2 = new Hand();
    hand2.addCard(new PlayingCard('D', 1));
    hand2.addCard(new PlayingCard('D', 2));
    hand2.addCard(new PlayingCard('D', 3));
    hand2.addCard(new PlayingCard('D', 4));
    hand2.addCard(new PlayingCard('D', 5));
    assertTrue(hand2.flush());
  }

  @Test
  @DisplayName("ContainsQueenOfSpades should return true")
  void containsQueenOfSpadesShouldReturnTrue() {
    hand.addCard(new PlayingCard('S', 12));
    assertTrue(hand.containsQueenOfSpades());
  }

  @Test
  @DisplayName("ContainsQueenOfSpades should return false")
  void containsQueenOfSpadesShouldReturnFalse() {
    assertFalse(hand.containsQueenOfSpades());
  }

  @Test
  void testToString() {
    assertEquals("C13 D7 H12 H5 S1", hand.toString());
  }

}
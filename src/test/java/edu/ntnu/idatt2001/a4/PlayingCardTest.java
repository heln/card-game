package edu.ntnu.idatt2001.a4;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PlayingCardTest {

  @Test
  @DisplayName("Face 0 should throw an IllegalArgumentException")
  void faceZeroShouldThrowIllegalArgumentException(){
    assertThrows(IllegalArgumentException.class, () -> new PlayingCard('H', 0));
  }

  @Test
  @DisplayName("Face 14 should throw an IllegalArgumentException")
  void faceFourteenShouldThrowIllegalArgumentException(){
    assertThrows(IllegalArgumentException.class, () -> new PlayingCard('H', 14));
  }

  @Test
  @DisplayName("Face 13 should be ok")
  void faceThirteenShouldBeOk(){
    PlayingCard playingCard = new PlayingCard('H', 13);
    assertEquals(13, playingCard.getFace());
  }

  @Test
  @DisplayName("Face 1 should be ok")
  void faceOneShouldBeOk(){
    PlayingCard playingCard = new PlayingCard('H', 1);
    assertEquals(1, playingCard.getFace());
  }


}
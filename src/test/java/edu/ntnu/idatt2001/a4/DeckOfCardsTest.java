package edu.ntnu.idatt2001.a4;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;


import static org.junit.jupiter.api.Assertions.*;

class DeckOfCardsTest {
  private DeckOfCards deckOfCards;

  @BeforeEach
  void generateInformation() {
    deckOfCards = new DeckOfCards();
  }

  @Test
  @DisplayName("Fifty two cards in a deck of cards")
  void fiftyTwoCardsInADeckOfCards(){
    assertEquals(52, deckOfCards.getDeckOfCards().size());
  }

  @Test
  @DisplayName("Playing card S1 should be the first element of deck of cards")
  void playingCardS1ShouldBeTheFirstCard() {
    PlayingCard playingCard = new PlayingCard('S', 1);
    assertEquals(playingCard, deckOfCards.getDeckOfCards().get(0));
  }

  @Test
  @DisplayName("Playing card C13 should be in deck of cards")
  void playingCardC13ShouldExist() {
    PlayingCard playingCard = new PlayingCard('C', 13);
    assertTrue(deckOfCards.getDeckOfCards().contains(playingCard));
  }

  @Test
  @DisplayName("Playing card C13 should be the last element of deck of cards")
  void playingCardC13ShouldBeTheLastCard() {
    PlayingCard playingCard = new PlayingCard('C', 13);
    assertEquals(playingCard, deckOfCards.getDeckOfCards().get(51));
  }

  @Test
  @DisplayName("Dealing a hand with less than 1 card should throw IllegalArgumentException")
  void dealHandWithZeroCards() {
    assertThrows(IllegalArgumentException.class, () -> deckOfCards.dealHand(0));
  }

  @Test
  @DisplayName("Dealing a hand with more than 52 cards should throw IllegalArgumentException")
  void dealHandWithFiftyThreeCards() {
    assertThrows(IllegalArgumentException.class, () -> deckOfCards.dealHand(53));
  }

  @Test
  @DisplayName("Dealing a hand of 5 cards")
  void dealHand(){
    Hand hand = deckOfCards.dealHand(5);
    assertEquals(5, hand.getCards().size());
  }
}
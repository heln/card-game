package edu.ntnu.idatt2001.a4;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.*;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class App extends Application {
  DeckOfCards deckOfCards;
  Hand hand;

  public static void main(String[] args) {
    launch(args);
  }

  @Override
  public void start(Stage primaryStage) {
    primaryStage.setTitle("Card-game");

    deckOfCards = new DeckOfCards();

    Button buttonDealHand = new Button("Deal hand");
    Button buttonCheckHand = new Button("Check hand");
    Text text = new Text("cards");


    HBox topMenu = new HBox(5);
    topMenu.getChildren().add(buttonDealHand);
    topMenu.getChildren().add(buttonCheckHand);


    Text sumDescription = new Text("Sum of faces: ");
    Text displaySum = new Text();
    Text heartsDescription = new Text("Cards of hearts: ");
    Text displayHearts = new Text();
    Text flushDescription = new Text("Flush: ");
    Text displayFlush = new Text();
    Text queenOfSpadesDescription = new Text("Cards of hearts: ");
    Text displayQueenOfSpades = new Text();

    buttonCheckHand.setOnAction(e -> {
      displaySum.setText(Integer.toString(hand.getSum()));
      displayHearts.setText(hand.getHearts());
      if (hand.flush()) {
        displayFlush.setText("Yes");
      } else displayFlush.setText("No");

      if (hand.containsQueenOfSpades()) {
        displayQueenOfSpades.setText("Yes");
      } else displayQueenOfSpades.setText("No");
    });

    buttonDealHand.setOnAction(e -> {
      hand = deckOfCards.dealHand(5);
      text.setText(hand.toString());

      displaySum.setText("");
      displayHearts.setText("");
      displayFlush.setText("");
      displayQueenOfSpades.setText("");

    });


    VBox bottom = new VBox(5);
    HBox firstLine = new HBox(5);
    HBox secondLine = new HBox(5);
    firstLine.getChildren().addAll(sumDescription, displaySum, heartsDescription, displayHearts);
    secondLine.getChildren().addAll(flushDescription, displayFlush, queenOfSpadesDescription, displayQueenOfSpades);
    bottom.getChildren().addAll(firstLine, secondLine);


    BorderPane borderPane = new BorderPane();
    borderPane.setTop(topMenu);
    borderPane.setCenter(text);
    borderPane.setBottom(bottom);


    Scene scene = new Scene(borderPane, 300, 250);
    primaryStage.setScene(scene);
    primaryStage.show();
  }

}

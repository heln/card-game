package edu.ntnu.idatt2001.a4;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Hand {
  private final ArrayList<PlayingCard> cards;

  /**
   * The constructor creates an empty list of cards.
   */
  public Hand(){
    cards = new ArrayList<>();
  }

  /**
   * Adds the given card to the list of cards that is on hand.
   *
   * @param card card
   */
  public void addCard(PlayingCard card){
    cards.add(card);
  }

  /**
   * Gets the list of cards on hand.
   *
   * @return list af cards
   */
  public ArrayList<PlayingCard> getCards(){
    return cards;
  }

  /**
   * Calculates the sum of the faces of the cards.
   *
   * @return sum
   */
  public int getSum(){
    return cards.stream()
        .map(PlayingCard::getFace)
        .reduce(Integer::sum)
        .get();
  }

  /**
   * Gets the cards on hand that have suit hearts
   * as a string sorted from greatest to lowest face.
   *
   * @return the cards on hand that are hearts as a string
   * Returns "No hearts" if there are no hearts on hand
   */
  public String getHearts() {
    String hearts = cards.stream()
        .filter(c -> 'H'==c.getSuit())
        .map(PlayingCard::toString)
        .sorted()
        .collect(Collectors.joining(" "));

    if (hearts.isBlank()) {
      return "No hearts";
    } else return hearts;
  }

  /**
   * Checkes if the card queen of spades (S12) is on hand.
   *
   * @return true if spades dame is on hand, and false if it isn't
   */
  public boolean containsQueenOfSpades() {
    List<PlayingCard> spareDame = cards.stream()
        .filter(c -> "S12".equals(c.toString()))
        .toList();
    return !spareDame.isEmpty();
  }

  /**
   * Checks if the hand has a 5-flush.
   * A 5-flush is when five (or more) of the cards is of the same suit.
   *
   * @return true if it's a flush and false if it isn't
   */
  public boolean flush() {
    char[] suits = { 'S', 'H', 'D', 'C' };

    for (char suit : suits) {
      List<PlayingCard> suitFlush = cards.stream()
          .filter(c -> suit==c.getSuit())
          .toList();
      if (suitFlush.size() >= 5) {
        return true;
      }
    }
    return false;
  }

  /**
   * Gets the hand of cards as a string
   * where the cards are sorted.
   *
   * @return cards on hand as a string
   */
  @Override
  public String toString() {
    return cards.stream()
        .map(PlayingCard::toString)
        .sorted()
        .collect(Collectors.joining(" "));
  }

}

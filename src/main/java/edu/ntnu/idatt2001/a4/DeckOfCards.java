package edu.ntnu.idatt2001.a4;

import java.util.ArrayList;
import java.util.Random;

public class DeckOfCards {
  private ArrayList<PlayingCard> deckOfCards;

  public DeckOfCards() {
    deckOfCards = new ArrayList<>();
    char[] suit = { 'S', 'H', 'D', 'C' };

    for (char c : suit) {
      for (int i = 1; i < 14; i++) {
        deckOfCards.add(new PlayingCard(c, i));
      }
    }
  }

  public ArrayList<PlayingCard> getDeckOfCards() {
    return deckOfCards;
  }

  public Hand dealHand(int n) throws IllegalArgumentException{
    if (n < 1 || n > 52) {
      throw new IllegalArgumentException("A hand cannot have less than 1 card or more than 52 cards.");
    }
    Random random = new Random();
    Hand hand = new Hand();

    while (hand.getCards().size() < n) {
      int number = random.nextInt(52);
      PlayingCard card = deckOfCards.get(number);
      if (!hand.getCards().contains(card)){
        hand.addCard(card);
      }
    }
    return hand;
  }

  @Override
  public String toString() {
    return "DeckOfCards = " + deckOfCards;
  }
}
